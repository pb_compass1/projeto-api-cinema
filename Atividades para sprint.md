## Lista das atividades para essa sprint
<br>

**Fase de planejamento**

1. [X] Analise da User Stories <br>
1.1 Analisar os requisitos<br>
1.2 Analisar os critérios de aceitação<br>
2. [X] Analise de testes<br>
2.1 Avaliar documentação disponivel pra API
3. [X] Mapa mental<br>
4. [X] Elaboração do plano de teste<br>
4.1 Analisar os Testes funcionais conforme mapa mental<br>
4.2 Analisar os Testes de performance conforme US<br>

**Fase de execução teste funcional**

5. [X] Elaboração dos ciclos de teste funcionais no JIRA<br>
6. [X] Elaboração de testes funcionais no Postman<br>
7. [X] Abertura de issues para demonstrar bugs funcionais (JIRA)<br>

**Fase de execução teste de performance**

8. [X] Elaboração dos scripts de teste usando a ferramenta k6<br>
9. [X] Realizar testes de performance.<br>




<!-- perguntas ao rafael.
1. Se eu tenho o swagger falando um cenarios (ex: status 201 para uma resposta) e tenho a User storie falando que a resposta deve ser outra (ex 200) qual eu devo seguir ? e como documentar isso ?
2. Na User Storie movies existe um requisito não funcional que não entendi A lista de filmes deve ser paginada, com no máximo 20 filmes por página. 
esstudar querry params para validar page 
o projeto é em uma branch nova ou posso fazer um projeto novo ?
se a user story preve que possa ter valor zero ele ainda sim é um bug ?
3. É para criar  uma nova branch ou pode ser criado um novo projeto ? -->

