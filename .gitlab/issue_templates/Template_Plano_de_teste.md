# **Plano de Teste**
### **1. Nome do projeto / User Storie:**<br>
    Descrever o nome do projeto / user storie.

### **2. Pessoas envolvidas:**<br>
    Descrever as pessoas que estão envolvidas nesse plano de teste.

### **3. Resumo/Escopo**<br>
    Descrever o resumos dos testes realizados.

### **4. Cobertura**<br>
    Ex:
    Fluxos de API de /usuarios, CRIA, LISTAR, ALTERAR E DELETAR (heuristica CRUD)
    ou
    Cobertura de verbo POST, PUT, GET, DELETE
    ou
    Cobertura de endepoint (casos onde tem apenas um verbo)

### **5. Cobertura de critérios de aceite:**<br>
    Referências os critérios de aceitação.
    EX:
    RF_001: Usuario deve conter o campo nome.
    RF_002: ......

### **6. Caso de teste:**<br>
    Especificar os testes que devem ser realizados e referência-los.
EX:
| CT    | Req.  | Prioridade    | Descrição |
| --- | --- | --- | --- |
|CT_001 |    - | Alta          | Criar um usuario com sucesso |
| CT_002 | RF_001 | Alta | Tentativa de cadastro de usuario sem nome |

### **7. Testes de performace:**<br>
    Descrever a lista dos testes de performace e seus requisitos.

