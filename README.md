# **Sobre**

Este projeto tem como objetivo realizar testes funcionais e não funcionais, para avaliar o desempenho da API cinema, disponibilizado [aqui](https://github.com/juniorschmitz/nestjs-cinema). O projeto teve inicio des do desenvolvimento de mapa mental, leitura da User Storie até testes funcionais realizados no Postman e testes não funcionais realziados com a ferramenta k6, seu objetivo é demonstrar aprendizado adiquirido. 

Nota: Os testes funcionais foram realizados em um servidor EC2, porem caso não possua esse recurso pdoe-se realizar normalmente utilizando host local.

## **Mensões**
Gostaria de agradecer a toda turma de bolsa de estudos da compass, com um agredecimento especial para:
* **Fabio Eloy** - Compartilhou uma solução para o requisito de paginação.
* **David Diogenes** - Realizamos varias calls para discutirmos logicas e melhores métodos de execução.
* **Arthur Melo** - Contribuiu com informações uteis nas daily dessa sprint.
* **Caue Mira** - Realizamos calls sobre logicas de algumas partes.
* **Jacques Schmit​​​​​​​z** - Por disponibilizar essa API para estudo.
* **Rafael Vescio** - Compartilhar os conteudos sobre o faker-js e servidores EC2.

## **Autor**
Filipe Alves

## **Tecnologias utilizadas**
* **JavaScript**
* **NodeJS**
* **k6**
* **faker-js**

## **Instruções de utilização**
### Pré-requisitos:
* Ter instaladas as ferramentas abaixo:
    * VSCode (ou o editor de código de sua escolha);
    * NodeJS;
    * Git.<br>

**Passo 1: Clonar o repositório**<br>
No terminal:<br>
    
    $ git clone https://gitlab.com/pb_compass1/projeto-api-cinema.git

**Passo 2: Instalar as dependências**<br>
**2.1 Instalar o k6**<br>
O k6 pode ser instalado em sistemas Unix-like (Linux e macOS) e Windows. Aqui será demonstrada a instalação no Windows. Para outros sistemas, acesse: https://k6.io/docs/getting-started/installation/.<br>
<br>
Abra o terminal na sua pasta raiz e execute os seguintes comandos:<br>

    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

Em seguida,<br>

    choco install k6

Certifique-se de seguir as instruções específicas do site para a instalação bem-sucedida do k6.

**2.2 Instalar o faker-js**<br>
No terminal, dentro da sua pasta raiz, execute o comando:

    npm install @faker-js/faker --save-dev

Isso instalará a biblioteca faker-js no seu projeto, permitindo que você a utilize para gerar dados fictícios nos seus testes de carga.

**Passo 3: Uso do k6 em testes**<br>
Abaixo, há um demonstrativo de como usar o k6 para fazer testes.<br>
<br>
Copie o código a seguir, cole-o em seu editor favorito e salve-o como script.js:

```javascript
import http from 'k6/http';
import { sleep } from 'k6';

export default function () {
  http.get('https://test.k6.io');
  sleep(1);
}
