# **Plano de Teste**
### **1. Nome do projeto / User Storie:**<br>
Projeto API cinema / US1 - Movies

### **2. Pessoas envolvidas:**<br>
Filipe Alves - QA

### **3. Resumo/Escopo**<br>
Realizar testes funcionais e não funcionais na rota /movies da API cinema, avaliando os requisitos propostos na User Storie US1 - Movies

### **4. Cobertura**<br>
Fluxos da API na rota /movies nos verbos GET, POST, DELETE e PUT.

### **5. Cobertura de critérios de aceite:**<br>
**Critérios de aceitação - funcionais**
| Ref.  | Descrição |
| --- | --- |
| CA1_01 | O usuário administrador da API envia uma solicitação POST para o endpoint /movies com os detalhes do filme. |
| CA1_02 | O sistema valida os campos obrigatórios e a unicidade do título. |
| CA1_03 | Se as validações passarem, o sistema cria o filme e atribui um ID único. |
| CA1_04 | O sistema retorna uma resposta de sucesso com o status 201 Created, incluindo o ID do filme. |
| CA1_05 | O usuário envia uma solicitação GET para o endpoint /movies. |
| CA1_06 | O sistema retorna uma lista de todos os filmes cadastrados com detalhes. |
| CA1_07 | O usuário envia uma solicitação GET para o endpoint /movies/{id}, onde {id} é o ID do filme desejado. |
| CA1_08 | O sistema verifica a existência do filme e retorna seus detalhes. |
| CA1_09 | Se o filme não existir, o sistema retorna uma resposta de erro com o status 404 Not Found. |
| CA1_10 | O usuário administrador da API envia uma solicitação PUT para o endpoint /movies/{id}, onde {id} é o ID do filme a ser atualizado. |
| CA1_11 | O sistema verifica a existência do filme, permite a atualização de campos específicos e valida os dados. |
| CA1_12 | Se todas as validações passarem, o sistema atualiza os detalhes do filme. |
| CA1_13 | O sistema retorna uma resposta de sucesso com o status 200 OK e os detalhes atualizados do filme. |
| CA1_14 | O usuário administrador da API envia uma solicitação DELETE para o endpoint /movies/{id}, onde {id} é o ID do filme a ser excluído. |
| CA1_15 | O sistema verifica a existência do filme e o remove permanentemente do banco de dados. |
| CA1_16 | O sistema retorna uma resposta de sucesso com o status 204 No Content. |

**Critérios de aceitação - não funcionais**
| Ref.  | Descrição |
| --- | --- |
| CA1_17 | A API deve ser capaz de processar pelo menos 100 solicitações de criação de filmes por segundo. |
| CA1_18 | O tempo médio de resposta para a criação de um novo filme não deve exceder 200 milissegundos. |
| CA1_19 | A API deve ser capaz de responder a solicitações GET de listagem de filmes em menos de 100 milissegundos. |
| CA1_20 | A lista de filmes deve ser paginada, com no máximo 20 filmes por página. |
| CA1_21 | A API deve ser capaz de responder a solicitações GET de detalhes de um filme em menos de 50 milissegundos. |
| CA1_22 | A API deve ser capaz de processar pelo menos 50 solicitações de atualização de filmes por segundo. |
| CA1_23 | O tempo médio de resposta para a atualização dos detalhes de um filme não deve exceder 300 milissegundos. |
| CA1_24 | A API deve ser capaz de processar pelo menos 30 solicitações de exclusão de filmes por segundo. |
| CA1_25 | O tempo médio de resposta para a exclusão de um filme não deve exceder 400 milissegundos. |

### **6. Caso de teste:**<br>

| CT    | Req.  | Prioridade    | Descrição |
| --- | --- | --- | --- |
| CT1_001 | CA1_05 e CA1_06 | -          | Lista todos os filmes com sucesso |
| CT1_002 | CA1_07 e CA1_08 | -          | Lista filme por id |
| CT1_003 | CA1_09 | -          | Tentativa de listar filme com id errado |
| CT1_004 | CA1_03 e CA1_04 | Alta     | Criar novo filme |
| CT1_005 | CA1_01 | Alta     | Tentativa de criar novo filme sem usuario adm |
| CT1_006 | CA1_02 | Alta     | Tentativa de criar filme sem um dos campos (campo vazio) |
| CT1_007 | CA1_02 | Alta     | Tentativa de criar filme com um campo em branco |
| CT1_008 | CA1_02 | Alta     | Tentativa de criar filme com um dos campos com apenas caractere especial (com apenas espaço ) |
| CT1_009 | CA1_10 e CA1_11 | Alta      | Atualizar filme como administrador |
| CT1_010 |    - | Alta       | Tentativa de atualizar filme sem usuario administrador |
| CT1_011 |    - | -          | Tentativa de atualizar filme com sem um dos campos (campo vazio) |
| CT1_012 |    - | -          | Tentativa de atualizar filme com um campo em branco |
| CT1_013 |    - | -          | Tentativa de atualizar filme com campo com apenas caractere especial (apenas espaço) |
| CT1_014 |   - | -          | Tentativa de atualizar um filme sem id | 
| CT1_015 |  -  | -          | Tentativa de atualizar um filme com id inexistente | 
| CT1_016 | CA1_14 e CA1_16 | Alta    | Deletar filme como administrador |
| CT1_017 | CA1_14 | Alta    | Tentativa de deletar filme como usuario comum |
| CT1_018 |    - | -         | Tentativa de deletar um filme com id inexistente |
| CT1_019 |    - | -         | Tentativa de deletar filme sem id |
| CT1_020 | CA1_02 | Alta    | Tentativa de criar um filme com um nome ja criado |
| CT1_021 | CA1_15 | Alta    | Tentativa de buscar por {id} um filme deletado |


### **7. Testes de performace:**<br>
| TP    | Ref.  | Descrição     | Métricas  |
| --- | --- | --- | --- |
| TP1_001 | -       | Realizar **Smoke testing** no fluxo de toda rota /movies | VUs: 3 <br>Duração: 1m |     
| TP1_002 | CA1_17 e CA1_18  | Realizar **Load testing** no verbo post /movies | VUs:100<br> Ramp-up:10%<br> Duração: 3min<br> Tempo de resposta: até 200ms |
| TP1_003 | -   | Realizar **Stress testing** no verbo POST /movies | VUs: 175<br> Ramp-up:20%<br> Duração: 3min<br> Tempo de resposta:200ms         |
| TP1_004 |CA1_19 e CA1_20 | Realizar **Load testing** no verbo GET /movies | VUs: 50<br> Ramp-up:10%<br> Duração: 3min<br> Tempo de resposta:100ms |
| TP1_005 |CA1_21  | Realizar **Load testing** no verbo GET{id} /movies | VUs: 50<br> Ramp-up:10%<br> Duração: 3min<br> Tempo de resposta:50ms |
| TP1_006 | CA1_22 e CA1_23 | Realizar **Load testing** no verbo PUT /movies | VUs: 50<br> Ramp-up:10%<br> Duração: 3min<br> Tempo de resposta:300ms |
| TP1_007 | CA1_24 e CA1_25 | Realizar **Load testing** no verbo DELETE /movies | VUs: 30<br> Ramp-up:10%<br> Duração:3min<br> Tempo de resposta:400ms |       



