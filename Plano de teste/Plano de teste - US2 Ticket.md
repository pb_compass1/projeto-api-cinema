# **Plano de Teste**
### **1. Nome do projeto / User Storie:**<br>
Projeto API cinema / US2 - Tickets

### **2. Pessoas envolvidas:**<br>
Filipe Alves - QA

### **3. Resumo/Escopo**<br>
Realizar testes funcionais e não funcionais na rota /tickets da API cinema, avaliando os requisitos propostos na User Storie US1 - Movies

### **4. Cobertura**<br>
Fluxos da API na rota /tickets nos verbos GET, POST, DELETE e PUT.

### **5. Cobertura de critérios de aceite:**<br>
**Critérios de aceitação - funcionais**
| Ref.  | Descrição |
| --- | --- |
| CA2_01 | O usuário envia uma solicitação POST para o endpoint /tickets com os seguintes detalhes do ingresso:<br> 1. ID do Filme (movieId) - Identifica o filme para o qual o ingresso está sendo reservado.<br> 2. ID do Usuário (userId) - Identifica o usuário que está fazendo a reserva.<br>3. Número do Assento (seatNumber) - O número do assento que o usuário deseja reservar.<br> 4. Preço do Ingresso (price) - O preço do ingresso para o filme.<br> 5. Data de Apresentação (showtime) - A data e hora da apresentação do filme.<br>O sistema valida se todos os campos obrigatórios estão preenchidos corretamente. |
| CA2_02 | O sistema verifica se o número do assento está dentro do intervalo de 0 a 99. |
| CA2_03 | O sistema verifica se o preço do ingresso está dentro do intervalo de 0 a 60. |
| CA2_04 | Se todas as validações passarem, o sistema cria uma reserva de ingresso com os detalhes fornecidos. |
| CA2_05 | O sistema atribui um ID único à reserva de ingresso. |
| CA2_06 | O sistema retorna uma resposta de sucesso com o status 201 Created, incluindo o ID da reserva de ingresso. |

**Critérios de aceitação - não funcionais**
| Ref.  | Descrição |
| --- | --- |
| CA2_07 | A API deve ser capaz de processar pelo menos 50 solicitações de reserva de ingressos por segundo. |
| CA2_08 | O tempo médio de resposta para a reserva de um ingresso não deve exceder 300 milissegundos. |

### **6. Caso de teste:**<br>

| CT    | Req.  | Prioridade    | Descrição |
| --- | --- | --- | --- |
| CT2_001 | -      | -      | Lista todos os ticekts com sucesso |
| CT2_002 | -      | -      | Lista ticket por id |
| CT2_003 | -      | -      | Tentativa de listar ticket com id errado |
| CT2_004 | CA2_01 | Alta   | Criar novo ticket |
| CT2_005 | CA2_01 | Alta   | Tentativa de criar ticket sem um dos campos (campo vazio) |
| CT2_006 | CA2_01 | Alta   | Tentativa de criar ticket com um campo em branco |
| CT2_007 | CA2_01 | Alta   | Tentativa de criar ticket com um dos campos com apenas caractere especial (com apenas espaço ) |
| CT2_008 | CA2_01 | Alta   | Tentativa de criar ticket com movie{id} inexistente |
| CT2_009 | CA2_02 | Alta   | Tentativa de criar ticket com numero de assento maior que 99 |
| CT2_010 | CA2_02 | Alta   | Tentativa de criar ticket com numero de assento menor que 0 |
| CT2_011 | CA2_03 | Alta   | Tentativa de criar ticket com preço menor que 0 | 
| CT2_012 | CA2_03 | Alta   | Tentativa de criar ticket com preço maior que 60 | 
| CT2_013 |  -     | -      | Tentativa de criar ticket com numero de assento iguais para o mesmo filme |
| CT2_014 |  -     |        | Atualizar ticket com sucesso |
| CT2_015 |  -     | -      | Tentativa de atualizar ticket com sem um dos campos (campo vazio) |
| CT2_016 |  -     | -      | Tentativa de atualizar ticket com um campo em branco |
| CT2_017 |  -     | -      | Tentativa de atualizar ticket com campo com apenas caractere especial (apenas espaço) |
| CT2_018 |  -     | -      | Tentativa de atualizar um ticket sem id do ticket | 
| CT2_019 |  -     | -      | Tentativa de atualizar um ticket com id do ticket inexistente | 
| CT2_020 |  -     | -      | Deletar ticket |
| CT2_021 |  -     | -      | Tentativa de deletar um ticket com id inexistente |
| CT2_022 |  -     | -      | Tentativa de deletar ticket sem id |
| CT2_023 |  -     | -      | Tentativa de buscar por {id} um ticket deletado |
| CT2_024 |  -     | -      | Tentativa de cadastro de ticket para showtime inexistente |

### **7. Testes de performace:**<br>
| TP    | Ref.  | Descrição     | Métricas  |
| --- | --- | --- | --- |
| TP2_001 | -   | Realizar **Smoke testing** no fluxo de toda rota /ticket | VUs: 3 <br>Duração: 1m |
| TP1_002 | CA1_07 e CA1_08  | Realizar **Load testing** no verbo post /tickets | VUs:50<br> Ramp-up:10%<br> Duração: 3min<br> Tempo de resposta: até 300ms |
| TP1_003 | CA1_07 e CA1_08  | Realizar **Stress testing** no verbo post /tickets | VUs:100<br> Ramp-up:20%<br> Duração: 3min<br> Tempo de resposta: até 300ms |



