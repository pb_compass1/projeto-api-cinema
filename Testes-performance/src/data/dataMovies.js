import { faker } from '@faker-js/faker';
import fs from 'fs'

export let listaMovies = [];

const qtd = 150;

for ( let i = 0; i <= qtd ; i++) {

    const bodyMovie = {
    title: faker.lorem.words(3),
    description: faker.lorem.paragraph(),
    launchdate: faker.date.past(), 
    showtimes: [faker.date.future().toISOString()]
  };
  listaMovies.push(bodyMovie)
}


fs.writeFileSync ("./Testes-performance/src/data/dataMovies.json", JSON.stringify(listaMovies, null, 2))