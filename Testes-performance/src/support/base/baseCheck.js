import { check } from 'k6';

export class BaseChecks {
    checkStatusCode(response, expectedStatus = [200, 201]){
        check( response, {
            [`expected status code of ${expectedStatus}`]: (r) => r.status === expectedStatus
        })
    }
    checkTimingsDuration(response, expectedDuration = 1000){
        check(response, {
            [`expected duration time of ${expectedDuration}ms`]: (r) => r.timings.duration < expectedDuration
        })
    }
    checkResponseSize(response, maxSize = 1000) {
        check(response, {
            'Response size is within limit': (r) => r.body.length <= maxSize,
        });
    }
    checkErrorRate(response, expectedRate = 0.05) {
        check(response, {
            'Check error rate' : (r) => r.error <= expectedRate,
        })
    }


}

