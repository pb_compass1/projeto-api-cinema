import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from "../base/baseTest.js";

const baseChecks = new BaseChecks();

export function randomString(length) {
    const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let rand = "";
    for (let i = 0; i < length; i++) {
        rand += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return rand;
}

export class TicketClass {
    constructor(baseRest, baseChecks) {
        this.baseRest = baseRest;
        this.baseChecks = baseChecks;
    }

    createTicket(payload) {
        return this.baseRest.post(ENDPOINTS.TICKETS_ENDPOINT, payload);
    }

    getAllTicket() {
        const res = this.baseRest.get(ENDPOINTS.TICKETS_ENDPOINT);
        return res;
    }

    getTicketById(ticketId) {
        return this.baseRest.get(`${ENDPOINTS.TICKETS_ENDPOINT}/${ticketId}`);
    }

    createOneTicket() {
        const payload = {
            movieId: "string",
            userI: "string",
            seatNumber: 0,
            price: 0,
            showtime: "2023-10-03T02:03:24.776Z"
        }
        return this.baseRest.post(ENDPOINTS.TICKETS_ENDPOINT, payload);
    }

    deleteTicketById(ticketId) {
        return this.baseRest.del(`${ENDPOINTS.TICKETS_ENDPOINT}/${ticketId}`);
    }

    updateTicketById(movieId) {
        const payload = {
            seatNumber: Math.floor(Math.random() * 60),
            price: Math.floor(Math.random() * 100),
            showtimes: "string"
        }
        return this.baseRest.put(`${ENDPOINTS.TICKETS_ENDPOINT}/${movieId}`, payload)
    }
}