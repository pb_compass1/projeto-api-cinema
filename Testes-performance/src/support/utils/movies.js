import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from "../base/baseTest.js";

const baseChecks = new BaseChecks();

export function randomString(length) {
    const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let rand = "";
    for (let i = 0; i < length; i++) {
        rand += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return rand;
}

export class MoviesClass {
    constructor(baseRest, baseChecks) {
        this.baseRest = baseRest;
        this.baseChecks = baseChecks;
    }

    createMovieFixed() {
        const payload = {
            title: "string",
            description: "string",
            launchdate: "2023-10-02T15:44:57.432Z",
            showtimes: [
              "string"
            ]
          }
        return this.baseRest.post(ENDPOINTS.MOVIES_ENDPOINT, payload);
    }

    createAllMovies(data) {
        return this.baseRest.post(ENDPOINTS.MOVIES_ENDPOINT, data);
    }

    getAllMovies() {
        const res = this.baseRest.get(ENDPOINTS.MOVIES_ENDPOINT);
        return res;
    }

    getMovieById(movieId){
        return this.baseRest.get(`${ENDPOINTS.MOVIES_ENDPOINT}/${movieId}`);
    }

    createOneMovie() {
        let titleRandom = randomString(15);
        const payload = {
            title: titleRandom,
            description: `Descrição de ${titleRandom}`,
            launchdate: "2023-10-02T15:44:57.432Z",
            showtimes: [
                "string"
            ]
        }
        return this.baseRest.post(ENDPOINTS.MOVIES_ENDPOINT, payload);
    }

    deleteMovieById(movieId) {
        return this.baseRest.del(`${ENDPOINTS.MOVIES_ENDPOINT}/${movieId}`);
    }

    updateMovieById(movieId) {
        let titleRandom = randomString(15);
        const payload = {
            title: titleRandom,
            description: `Alterado ${titleRandom}`,
            launchdate: "2023-10-02T15:44:57.432Z",
            showtimes: [
                "string"
            ]
        }
        return this.baseRest.put(`${ENDPOINTS.MOVIES_ENDPOINT}/${movieId}`, payload)
    }
}