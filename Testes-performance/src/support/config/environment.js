export const testConfig = {
    environment: {
        hml: {
            url: "http://35.175.185.152:3000"
        }
    },
    options: {
        test: {
        vus: 1,
        duration: '10s',
        thresholds: {
            http_req_duration: ['p(95)<2000'],
            http_req_failed: ['rate<0.05']
        }
    },
        smokeTestGeneral: {
            vus: 3,
            duration: '1m',
            thresholds: {
                http_req_duration: ['p(95)<300'],
                http_req_failed: ['rate<0.05']
            }
        },
        loadTestTP1_002: {
            stages: [
                { duration: '20s', target: 100 },
                { duration: '140s', target: 100 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<200'],
                http_req_failed: ['rate<0.05']
            }
        },
        stressTestTP1_003: {
            stages: [
                { duration: '60s', target: 175 },
                { duration: '140s', target: 175 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<200'],
                http_req_failed: ['rate<0.05']
            }
        },
        loadTestTP1_004: {
            stages: [
                { duration: '20s', target: 50 },
                { duration: '140s', target: 50 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<100'],
                http_req_failed: ['rate<0.05']
            }
        },
        loadTestTP1_005: {
            stages: [
                { duration: '20s', target: 50 },
                { duration: '140s', target: 50 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<50'],
                http_req_failed: ['rate<0.05']
            }
        },
        loadTestTP1_006: {
            stages: [
                { duration: '20s', target: 50 },
                { duration: '140s', target: 50 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<300'],
                http_req_failed: ['rate<0.05']
            }
        },
        loadTestTP1_007: {
            stages: [
                { duration: '20s', target: 50 },
                { duration: '140s', target: 50 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<400'],
                http_req_failed: ['rate<0.05']
            }
        },
        loadTestTP2_001: {
            stages: [
                { duration: '20s', target: 50 },
                { duration: '140s', target: 50 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<300'],
                http_req_failed: ['rate<0.05']
            }
        },
        stressTestTP2_003: {
            stages: [
                { duration: '20s', target: 100 },
                { duration: '140s', target: 100 },
                { duration: '20s', target: 0 },
              ],
            thresholds: {
                http_req_duration: ['p(95)<300'],
                http_req_failed: ['rate<0.05']
            }
        },
    }
}
