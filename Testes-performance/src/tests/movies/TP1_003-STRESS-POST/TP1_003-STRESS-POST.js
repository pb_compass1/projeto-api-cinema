import { sleep } from "k6"
import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js";
import { MoviesClass } from "../../../support/utils/movies.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {"TP1-002LOAD-POST.html": htmlReport(data)};
}
export const options = testConfig.options.stressTestTP1_003;

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const movies = new MoviesClass(baseRest, baseChecks)

export default function () {
    
    const resPost = movies.createOneMovie();

    baseChecks.checkStatusCode(resPost, 201);
    baseChecks.checkTimingsDuration(resPost, 200)
    baseChecks.checkResponseSize(resPost)
    baseChecks.checkErrorRate(resPost)
   
    sleep(1);
}

export function teardown() {
    const resGet = movies.getAllMovies().json();
    resGet.map(item => {
        const resDel = movies.deleteMovieById(item._id)
        baseChecks.checkStatusCode(resDel, 200)
    })

    movies.createMovieFixed();

}