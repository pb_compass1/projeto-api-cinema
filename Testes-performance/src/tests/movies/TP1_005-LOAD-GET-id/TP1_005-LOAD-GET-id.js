import { sleep } from "k6"
import { SharedArray } from 'k6/data'
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from "../../../support/base/baseTest.js";
import { MoviesClass } from "../../../support/utils/movies.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {"TP1-005-LOAD-GET-id.html": htmlReport(data)};
}

export const options = testConfig.options.test;

const dataMovie = new SharedArray('Products', function () {
    const jsonData = JSON.parse(open('../../../data/dataMovies.json'));
    return jsonData
})

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const movies = new MoviesClass(baseRest, baseChecks)

export function setup() {
    for (let i = 0; i < dataMovie.length; i++) {
        const resPost = movies.createAllMovies(dataMovie[i]);
        baseChecks.checkStatusCode(resPost, 201)
    }
    const responseData = movies.getAllMovies().json();
    return responseData
}

export default function (responseData) {

    const movieIndex = Math.floor(Math.random() * responseData.length);
    const movie = responseData.splice(movieIndex, 1)[0]

    const resGet = movies.getMovieById(movie._id)

    baseChecks.checkStatusCode(resGet, 200);
    baseChecks.checkTimingsDuration(resGet, 50)
    baseChecks.checkResponseSize(resGet)
    baseChecks.checkErrorRate(resGet)
 
    sleep(1);
}

export function teardown() {
    const resGet = movies.getAllMovies().json();
    resGet.map(item => {
        const resDel = movies.deleteMovieById(item._id)
        baseChecks.checkStatusCode(resDel, 200)
    })

    movies.createMovieFixed();

}