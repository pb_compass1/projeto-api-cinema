import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js"
import { MoviesClass } from "../../../support/utils/movies.js";
import { group, sleep } from 'k6';
import postMovie from '../scenarios/postMovie.js'
import getMovie from '../scenarios/getMovies.js'
import putMovie from '../scenarios/putMovie.js'
import deleteMovie from "../scenarios/deleteMovie.js"
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {"TP1-001-SMOKE.html": htmlReport(data)};
}

export const options = testConfig.options.smokeTestGeneral;

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();
const movies = new MoviesClass(baseRest, baseChecks)

export default function() {
    group('Endpoint POST movies - Criar filme - API Cinema', () => {
        postMovie();
    });
    sleep(1);
    
    group('Endpoint GET movies - Listar filme - API Cinema', () => {
        getMovie();
    });
    sleep(1);

    group('Endpoint PUT movies - Editar filme - API Cinema', () => {
        putMovie();
    });
    sleep(1);

    group('Endpoint DELETE movies - Deletar filme - API Cinema', () => {
        deleteMovie();
    });

    sleep(1);
}
