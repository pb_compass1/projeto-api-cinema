import { sleep } from "k6"
import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js";
import { MoviesClass } from "../../../support/utils/movies.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {"TP1-004-LOAD-GET.html": htmlReport(data)};
}

export const options = testConfig.options.loadTestTP1_004;

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const movies = new MoviesClass(baseRest, baseChecks)

export default function () {
    
    const resGet = movies.getAllMovies();
    
    /* Código compartilhado pelo Fabio Eloy, porem não consegui fazer funcionar
    // const maxPerPage = 20;
    // const res = BaseRest.get(ENDPOINTS.MOVIES_ENDPOINT+`?page=1&pageSize=${maxPerPage}`);
    */
    baseChecks.checkStatusCode(resGet, 200);
    baseChecks.checkTimingsDuration(resGet, 100)
    baseChecks.checkErrorRate(resGet)
   
    sleep(1);
}
