import { sleep } from "k6"
import { SharedArray } from 'k6/data'
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from "../../../support/base/baseTest.js";
import { MoviesClass } from "../../../support/utils/movies.js";

const dataMovie = new SharedArray('Products', function () {
    const jsonData = JSON.parse(open('../../../data/dataMovies.json'));
    return jsonData
})

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const movies = new MoviesClass(baseRest, baseChecks)

export default function () {
    
    const resPost = movies.createOneMovie(dataMovie)

    baseChecks.checkStatusCode(resPost, 201);
    baseChecks.checkTimingsDuration(resPost, 300);
    
    // sleep(1);
}