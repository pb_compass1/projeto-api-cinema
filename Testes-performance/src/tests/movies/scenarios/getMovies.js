import { sleep } from "k6"
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from "../../../support/base/baseTest.js";
import { MoviesClass } from "../../../support/utils/movies.js";

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const movies = new MoviesClass(baseRest, baseChecks)

export default function () {

    const resGet = movies.getAllMovies();

    baseChecks.checkStatusCode(resGet, 200);
    baseChecks.checkTimingsDuration(resGet, 500);

    // sleep(1);
}