import { sleep } from "k6"
import { SharedArray } from 'k6/data'
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from "../../../support/base/baseTest.js";
import { MoviesClass } from "../../../support/utils/movies.js";

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const movies = new MoviesClass(baseRest, baseChecks)

export default function () {
    
    const data = movies.getAllMovies().json();
    const movieIndex = __VU % data.length;
    const movie = data[movieIndex];

    const resDel = movies.deleteMovieById(movie._id)
    baseChecks.checkStatusCode(resDel, 200);
    baseChecks.checkTimingsDuration(resDel, 300);

    // sleep(1);
}

