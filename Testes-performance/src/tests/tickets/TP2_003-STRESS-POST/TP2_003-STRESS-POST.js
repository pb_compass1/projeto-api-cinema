import { sleep } from "k6"
import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js";
import { TicketClass } from "../../../support/utils/tickets.js";
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/main/dist/bundle.js";

export function handleSummary(data) {
    return {"TP2-003-STRESS-POST.html": htmlReport(data)};
}

export const options = testConfig.options.stressTestTP2_003;

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const tickets = new TicketClass(baseRest, baseChecks)

export default function () {
    const movieId = "string"
    const userId = "string"
    const payload = {
        movieId: movieId,
        userId: userId,
        seatNumber: Math.floor(Math.random() * 60),
        price: Math.floor(Math.random() * 100),
        showtimes: "string"
    }
    const resPost = tickets.createTicket(payload)

    baseChecks.checkStatusCode(resPost, 201);
    baseChecks.checkTimingsDuration(resPost, 300);
    baseChecks.checkResponseSize(resPost)
    baseChecks.checkErrorRate(resPost)
   
    sleep(1);
}

export function teardown() {
    // const resGet = tickets.getAllTicket().json();
    // resGet.map(item => {
    //     const resDel = tickets.deleteTicketById(item._id)
    //     baseChecks.checkStatusCode(resDel, 200)
    // })

    // tickets.createOneTicket();

}