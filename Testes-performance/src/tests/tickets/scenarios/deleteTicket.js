import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js";
import { TicketClass } from "../../../support/utils/tickets.js";

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const tickets = new TicketClass(baseRest, baseChecks)

export default function () {
    
    const data = tickets.getAllTicket().json();
    const ticketIndex = __VU % data.length;
    const ticket = data[ticketIndex];

    const resDel = tickets.deleteTicketById(ticket._id)
    baseChecks.checkStatusCode(resDel, 200);
    baseChecks.checkTimingsDuration(resDel, 300);

}

