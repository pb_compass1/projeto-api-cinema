import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js";
import { TicketClass } from "../../../support/utils/tickets.js";

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const tickets = new TicketClass(baseRest, baseChecks)

export default function () {

    const resGet = tickets.getAllTicket();

    baseChecks.checkStatusCode(resGet, 200);
    baseChecks.checkTimingsDuration(resGet, 500);

}