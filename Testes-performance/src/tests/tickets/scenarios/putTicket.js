import { sleep } from "k6"
import { SharedArray } from 'k6/data'
import { BaseChecks, BaseRest, ENDPOINTS, testConfig } from "../../../support/base/baseTest.js";
import { MoviesClass } from "../../../support/utils/movies.js";

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const movies = new MoviesClass(baseRest, baseChecks)

export function setup () {
    const data = movies.getAllMovies();
    return data
}

export default function () {

    const data = movies.getAllMovies().json();
    const movieIndex = __VU % data.length;
    const movie = data[movieIndex];
    
    const resPut = movies.updateMovieById(movie._id)
    baseChecks.checkStatusCode(resPut, 200);
    baseChecks.checkTimingsDuration(resPut, 300);

    // sleep(1);
}

