import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js";
import { TicketClass } from "../../../support/utils/tickets.js";

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();

const tickets = new TicketClass(baseRest, baseChecks)

export default function () {
    const movieId = "string"
    const userId = "string"
    const payload = {
        movieId: movieId,
        userId: userId,
        seatNumber: Math.floor(Math.random() * 60),
        price: Math.floor(Math.random() * 100),
        showtimes: "string"
    }
    const resPost = tickets.createTicket()

    baseChecks.checkStatusCode(resPost, 201);
    baseChecks.checkTimingsDuration(resPost, 300);
}