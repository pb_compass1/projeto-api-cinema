import { BaseChecks, BaseRest, testConfig } from "../../../support/base/baseTest.js"
import { ticketsClass } from "../../../support/utils/tickets.js";
import { group, sleep } from 'k6';
import { TicketClass } from "../../../support/utils/tickets.js";
import postTicket from "../scenarios/postTicket.js";
import getTicket from "../scenarios/getTicket.js";
import deleteTicket from "../scenarios/deleteTicket.js";
import postTicket from "../scenarios/postTicket.js";

export const options = testConfig.options.smokeTestGeneral;

const baseRest = new BaseRest(testConfig.environment.hml.url);
const baseChecks = new BaseChecks();
const tickets = new TicketClass(baseRest, baseChecks)

export default function() {
    group('Endpoint POST Tickets - Criar ticket - API Cinema', () => {
        postTicket();
    });
    sleep(1);
    
    group('Endpoint GET Tickets - Listar ticket - API Cinema', () => {
        getTicket();
    });
    sleep(1);

    group('Endpoint PUT Tickets - Editar ticket - API Cinema', () => {
        putTicket();
    });
    sleep(1);

    group('Endpoint DELETE Tickets - Deletar ticket - API Cinema', () => {
        deleteTicket();
    });

    sleep(1);
}
