import http from 'k6/http';
import { BaseService } from "./baseServices.js";


export class BaseRest extends BaseService {
    constructor(base_uri) {
        super(base_uri);
    }

    post(endpoint, body, headers = {}, params = {}){
        let uri = this.base_uri + endpoint;
        let options =this.buildOptions(headers, params);
        return http.post(uri, JSON.stringify(body), options)
    }

    get(endpoint, body, headers = {}, params = {}){
        let uri = this.base_uri + endpoint;
        let options =this.buildOptions(headers, params);
        return http.get(uri, JSON.stringify(body), options)
    }

    del(endpoint, body, headers = {}, params = {}){
        let uri = this.base_uri + endpoint;
        let options =this.buildOptions(headers, params);
        return http.del(uri, JSON.stringify(body), options)
    }

    put(endpoint, body, headers = {}, params = {}){
        let uri = this.base_uri + endpoint;
        let options =this.buildOptions(headers, params);
        return http.put(uri, JSON.stringify(body), options)
    }

    buildOptions(headers = {}, params = {}, token ={}) {
        return {
            headers: Object.assign({'Content-Type': 'application/json'}, headers),
            params: Object.assign({'monitor': 'false'}, params),
            authorization: Object.assign({'uthorization': `${token}`}, token) 
        }
    }

}

